package ru.nlmk.study.jse37.java10.ex01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws IOException {
        var codefx = new URL("http://someurl.com");
        var connection = codefx.openConnection();
        var reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
    }
}
