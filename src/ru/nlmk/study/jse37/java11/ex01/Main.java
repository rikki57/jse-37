package ru.nlmk.study.jse37.java11.ex01;

import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        String str = "";
/*        System.out.println(str.isBlank());
        str = "  null ";
        System.out.println(str.isBlank());*/

/*        str = "some";
        System.out.println(str.repeat(10));*/

/*        String lines = "sfsdfsdf \n sdfsfdsdfsdfs  \r\n  fsdfsdfsfdlksjfd  \n";
        lines.lines().forEach(s -> System.out.println(s.strip()));*/

        List<String> list = List.of("1", "2", "3", "4");
        list.stream()
                .map((@NonNull var s) -> s.toLowerCase())
                .collect(Collectors.toList());
    }
}
