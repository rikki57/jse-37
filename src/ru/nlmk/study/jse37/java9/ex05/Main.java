package ru.nlmk.study.jse37.java9.ex05;

import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
/*        Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .takeWhile(n -> n < 5)
                .forEach(System.out::println);*/

/*        Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .dropWhile(n -> n < 5)
                .forEach(System.out::println);*/

        IntStream.iterate(0, n -> n + 1)
                .forEach(System.out::println);
    }
}
