package ru.nlmk.study.jse37.java9.ex04;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Main {
    private static Map<String, String> racers = Map.of(
            "Mick", "Schumacher",
            "Nikita", "Mazepin",
            "Charles", "Leclerc"
    );

    public static void main(String[] args) {
        List<String> names = List.of("Mick", "Nikita", "Charles", "Sebastian", "Lewis");
        names.stream()
                .map(Main::getSurname)
                .flatMap(Optional::stream)
                .forEach(System.out::println);
    }

    private static Optional<String> getSurname(String name) {
        return Optional.ofNullable(racers.get(name));
    }
}
