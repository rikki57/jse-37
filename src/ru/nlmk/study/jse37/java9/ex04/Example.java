package ru.nlmk.study.jse37.java9.ex04;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Example {
    private static Map<String, String> hasTeam = Map.of(
            "Mick", "Schumacher",
            "Nikita", "Mazepin",
            "Charles", "Leclerc",
            "Sebastian", "Vettel"
    );

    public static void main(String[] args) {
        List<String> names = List.of("Mick", "Nikita", "Charles", "Sebastian", "Lewis");

        names.stream()
                .map(Example::getSurname)
                .forEach(s -> s.ifPresentOrElse(Example::printHasSeat, Example::printHasNoSeat));
    }

    private static void printHasSeat(String surName) {
        System.out.println(surName + " has a seat");
    }

    private static void printHasNoSeat() {
        System.out.println("Someone has no seat");
    }

    private static Optional<String> getSurname(String name) {
        return Optional.ofNullable(hasTeam.get(name));
    }
}
