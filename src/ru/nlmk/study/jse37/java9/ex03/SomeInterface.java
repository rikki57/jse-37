package ru.nlmk.study.jse37.java9.ex03;

public interface SomeInterface {
    static void method3() {
        method5();
        System.out.println("Static method");
    }

    private static void method5() {
        System.out.println("Private static method");
    }

    public abstract void method1();

    public default void method2() {
        method4();
        method5();
        System.out.println("Default method");
        System.out.println("");
    }

    private void method4() {
        System.out.println("Private method");
    }
}
