package ru.nlmk.study.jse37.java9.ex03;

public class Main {
    public static void main(String[] args) {
        SomeClass someClass = new SomeClass();
        someClass.method1();
        someClass.method2();
        SomeInterface.method3();
    }
}
