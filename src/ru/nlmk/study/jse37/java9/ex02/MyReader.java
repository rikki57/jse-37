package ru.nlmk.study.jse37.java9.ex02;

public class MyReader implements AutoCloseable {
    @Override
    public void close() throws Exception {
        System.out.println("Close called");
    }
}
