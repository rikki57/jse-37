package ru.nlmk.study.jse37.java9.ex02;

public class MyReaderService {
    public String read(String filename) {
        MyReader reader = new MyReader();
        try (reader) {
            System.out.println("Work with resource");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "result";
    }
}
