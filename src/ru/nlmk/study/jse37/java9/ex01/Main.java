package ru.nlmk.study.jse37.java9.ex01;

import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        List<String> immutableList = List.of("One", "Two", "Three");
        immutableList.forEach(System.out::println);

        Map<Integer, String> immutableMap = Map.of(1, "One", 2, "Two", 3, "Three");
        immutableMap.put(4, "Four");
    }
}
